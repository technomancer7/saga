Example [testing example happy]
This is an example Saga markup file.
Each "entry" is split with a line that contains only ---, this line is not split because the line contains text.
The first line of each block is its title.

In the title, the bracketed words are the entries tags, useful for sorting and searching.
---
