import opthandler, colours
import os, osproc, json, strutils, sequtils, tables, strformat, times

type
    Saga = object
        title, body: string
        tags: seq[string]
let 
    home = getHomeDir()&".saga.d/"
    data = home&"data/"
    opt = newOptHandler()
    dontUseColours = opt.hasFlag("c")
    exportJson = opt.hasFlag("j")
    
proc figlet*(s: string) = discard execShellCmd("figlet -k -c "&s)

proc getFileName(): string =
    var n = now().format("dd-MM-yyyy")

    if opt.hasFlag("date"): 
        n = opt.flag("date")
        try:
            let dt = parse(n, "dd-MM-yyyy")
        except:
            echo "Invalid format for --date value. Example: "&now().format("dd-MM-yyyy")
            return
        
    return n&".txt"  

proc hasTag(s: Saga, aTwo: seq[string]): bool =
    for item in s.tags:
        if item in aTwo:
            return true
        
    return false
proc parseSagaFile(name:string): seq[Saga] =
    let text = readFile(data&name)
    for entry in text.split("---"):
        let lns = entry.strip().split("\n")
        var nx = Saga()
        nx.title = lns[0]
        nx.body = lns[1..^1].join("\n").strip()
        if "[" in nx.title and "]" in nx.title:
            nx.tags = nx.title.split("[")[1].split("]")[0].split(" ")
            nx.title = nx.title.split(" [")[0]
        result.add nx
        
proc main() =
    if not fileExists(home&"saga.json"): writeFile(home&"saga.json", "{}")
    discard existsOrCreateDir(home&"data/")
    
    let 
        cfg = parseFile(home&"saga.json")
        writer = cfg{"write"}.getStr("nano")
    
    case opt.command(0, "help")
    of "new", "write", "edit":
        if opt.hasFlag("h"): echo "new/write\nWrite a new entry.\nDefaults to current date, override and write for other date with --date=dd-MM-yyyy"; return
        let fname = getFileName()
        if not fileExists(data&fname): writeFile(data&fname, "")
        discard execShellCmd(writer&" "&data&fname)
        
    of "set":
        if opt.hasFlag("h"): echo "set <key> <value...>\nSets a config key."; return
        if opt.commands.len < 2:
            echo "Error".red&" Requires more information\nset <key> <value...>";return
        cfg[opt.command(1)] = opt.commands[2..^1].join(" ").newJString
        writeFile(home&"saga.json", $cfg)
        
    of "unset":
        if opt.hasFlag("h"): echo "unset <key>\nDeletes a config key"; return
        if opt.commands.len < 1:
            echo "Error".red&" Requires more information\nunset <key>";return
        cfg.delete opt.command(1)
        writeFile(home&"saga.json", $cfg)
        
    of "get":
        if opt.hasFlag("h"): echo "get <key>\nPrints a config key.\nPrints all values if no key given."; return
        if opt.command(1, "") == "":
            for key, val in cfg.pairs:
                echo key.blue&" = "&val.getStr("None").blue 
        else:
            echo cfg[opt.command(1)]
        
    of "del", "delete":
        let fname = getFileName()
        
    of "search":
        if opt.hasFlag("h"): echo "search [title] [--t:tag1,tag2..]\nSearches for entries matching title, or containing one of the tags."; return
        var f = 0
        let fnd_t = opt.flag("t").split(",")
        let fnd_title = opt.commands[1..^1].join(" ").strip()

        for kind, path in walkDir(data):
            if (kind == pcFile or kind == pcLinkToFile) and not path.endsWith("~"):
                let s = parseSagaFile(path.split("/")[^1])
        
                for en in s:
                    if (fnd_title != "" and fnd_title in en.title) or en.hasTag(fnd_t):
                        echo "=============="
                        echo en.title.green&" ["&en.tags.join(" ").blue&"] from "&path.split("/")[^1].split(".")[0]
                        echo en.body
                        f += 1
                    
        if f > 0:
            echo "=============="
        else:
            echo "No results found"
            
    of "list", "ls":
        if opt.hasFlag("h"): echo "list\nShows all dates that have entries."; return
        for kind, path in walkDir(data):
            if (kind == pcFile or kind == pcLinkToFile) and not path.endsWith("~"):
                echo(path.split("/")[^1].split(".")[0])
                
    of "show", "print":
        if opt.hasFlag("h"): echo "show [--date:dd-MM-yyyy]\nShows all entries in a date. Uses NOW if no date given."; return
        let fname = getFileName()
        if not fileExists(data&fname): echo "Date does not exist."; return
        let s = parseSagaFile(fname)
        echo "=============="
        for en in s:
                echo en.title.green&" ["&en.tags.join(" ").blue&"]"
                echo en.body
                echo "=============="
                    
    of "example":
        echo readFile(home&"example.txt")
        
    of "help":
        figlet "SAGA"
        echo "CLI journal application"
        echo ""
        echo "Commands:"
        echo "new".blue
        echo "set".blue
        echo "unset".blue
        echo "get".blue
        echo "edit".blue
        echo "search".blue
        echo "list".blue
        echo "show".blue
        echo "example".blue
        echo "help".blue&" <-- Here"
    
    
main()
