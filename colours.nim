 

proc black*(s: string): string = return "\u001b[30m"&s&"\u001b[0m"
proc red*(s: string): string = return "\u001b[31m"&s&"\u001b[0m"
proc green*(s: string): string = return "\u001b[32m"&s&"\u001b[0m"
proc yellow*(s: string): string = return "\u001b[33m"&s&"\u001b[0m"
proc blue*(s: string): string = return "\u001b[34m"&s&"\u001b[0m"
proc magenta*(s: string): string = return "\u001b[35m"&s&"\u001b[0m"
proc cyan*(s: string): string = return "\u001b[36m"&s&"\u001b[0m"
proc white*(s: string): string = return "\u001b[37m"&s&"\u001b[0m"

proc brightblack*(s: string): string = return "\u001b[30;1m"&s&"\u001b[0m"
proc brightred*(s: string): string = return "\u001b[31;1m"&s&"\u001b[0m"
proc brightgreen*(s: string): string = return "\u001b[32;1m"&s&"\u001b[0m"
proc brightyellow*(s: string): string = return "\u001b[33;1m"&s&"\u001b[0m"
proc brightblue*(s: string): string = return "\u001b[34;1m"&s&"\u001b[0m"
proc brightmagenta*(s: string): string = return "\u001b[35;1m"&s&"\u001b[0m"
proc brightcyan*(s: string): string = return "\u001b[36;1m"&s&"\u001b[0m"
proc brightwhite*(s: string): string = return "\u001b[37;1m"&s&"\u001b[0m"

proc fg*(s, foreground: string): string = return "\u001b[38;5;"&foreground&"m"&s&"\u001b[0m"

proc bg*(s, bground: string): string = return "\u001b[48;5;"&bground&"m"&s&"\u001b[0m"

proc bold*(s: string): string = return "\u001b[1m"&s&"\u001b[0m"
proc underline*(s: string): string = return "\u001b[4m"&s&"\u001b[0m"
proc resetfmt*(s: string): string = return "\u001b[10"&s&"\u001b[0m"
